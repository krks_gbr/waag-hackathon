




import path from 'path';
import Express from 'express';
import compression from 'compression';
import bodyParser from 'body-parser';
import handleRender from './ssr';

export default ({port, host, callback}) => {

    console.log(port, host);
    const app = Express();
    app.use(bodyParser.json());
    app.use(compression());
    app.use( Express.static( path.join(appRoot, 'dist/public') ));
    app.use(handleRender);


    app.listen(port, host, () => {
        console.log(`server is listening at http://${host}:${port}`);
        if(callback){
            callback();
        }
    });

}



