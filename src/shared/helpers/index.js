


export function calculateScoreForSchool(school){
    console.log(school);
    if(school.reviews.length)
        return school.reviews.map(r => r.accuracy).reduce((a, b)=>a+b) / school.reviews.length;


    return -1;
}