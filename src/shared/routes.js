


import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './app';
import IndexView from './views/index';
import DefaultLayout from './layouts/default';
import Reviews from './views/reviews';
import SearchResults from './views/search-results';


export default

    <Route path="/" component={App}>
        <Route component={DefaultLayout}>
            <IndexRoute component={IndexView}/>
            {/*<Route path="/one" component={ViewOne}/>*/}
            {/*<Route path="/two" component={ViewTwo}/>*/}
            <Route path="/reviews/:school_id" component={Reviews}/>
            <Route path="/search" component={SearchResults}/>
        </Route>
    </Route>

;
