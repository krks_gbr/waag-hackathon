



import React from 'react';
import '../global-style/style.scss';
import Loading from '../views/loading';

import {connect} from 'react-redux';


class App extends React.Component {
    render () {
        if(!this.props.rehydrated){
            return <Loading/>;
        }
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}

export default connect(state => ({
    rehydrated: state.rehydrated
}))(App);



