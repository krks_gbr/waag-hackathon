

import style from './style.scss'
import React from 'react';
import Header from '../../components/header';
import Footer from '../../components/footer';


export default props => {


    return (
        <div className={style.defaultLayout}>
            <Header/>
            <div className={style.body}>
                {props.children}
            </div>
            <Footer/>
        </div>
    );


};

