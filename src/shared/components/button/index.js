
import style from './style.scss'
import React from 'react';
import classNames from 'classnames';


export default props => {

    let {disabled, fill, onClick, label, className} = props;

    let buttonClassName = disabled ? style.disabled : style.button;
    buttonClassName = fill ? classNames(style.fill, buttonClassName) : buttonClassName;

    let wrapperClassName = className ? className : style.wrapper;
    wrapperClassName = fill ? classNames(style.fill, wrapperClassName) : wrapperClassName;

    return (
            <div className={wrapperClassName}>
                <button className={buttonClassName}
                        onClick={disabled ? null : onClick}>
                    {label}
                </button>
            </div>
    )
}
