



import React from 'react';
import Select from 'react-select';
import {connect} from 'react-redux';
import _ from 'lodash';
import  style from './style.scss'
import Button from '../../components/button';




class SearchBar extends React.Component{


    state = {
        postCode: "",
        distance: null,
        level: null,
        subject: null,
    };

    setPostCode = postCode => {
        this.setState({postCode});
    };

    setSubjects = subject => {
        this.setState({subject});
    };

    setLevel = level => {
        this.setState({level});
    };

    setDistance = distance => {
        this.setState({distance});
    };

    initSearch = () => {

    };


    render(){


        const createOptions = list => {
            return list.map(item => ({label: item, value: item}))
        };

        const {levels, subjects} = this.props;


        const levelsOptions = createOptions(levels.map(l => l.toUpperCase()));
        const subjectOptions = createOptions(subjects);
        const distanceOptions = [5, 10, 20, 30, 50, 100].map(d => {
            return {
                label: `< ${d}km`,
                value: d,
            }
        });


        return (
            <div className={style.search}>
                <div className={style.inputContainer}>
                    <input className={style.input} value={this.state.postCode} onChange={e => this.setPostCode(e.target.value)}/>
                </div>
                <Select
                    name="distance"
                    options={distanceOptions}
                    value={this.state.distance}
                    onChange={this.setDistance}
                />
                <Select
                    name="levels"
                    options={levelsOptions}
                    value={this.state.level}
                    onChange={this.setLevel}
                />
                <Select
                    name="subject"
                    options={subjectOptions}
                    value={this.state.subject}
                    onChange={this.setSubjects}
                />
                <Button label="Search" onClick={() => console.log("button clicked")}/>
            </div>
        );
    }
}





const select = state => {

    const extractFromSubjects = key =>
        _.chain(state.schools)
            .map(s => s.subjects)
            .map(s => s.map( si => si[key]  ))
            .flatten()
            .uniq()
            .value();


    const levels = extractFromSubjects('level');
    const subjects = extractFromSubjects('name');

    
    return {
        levels,
        subjects,
    }
};

export default connect(select)(SearchBar);

