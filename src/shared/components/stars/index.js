


import React from 'react';
import {calculateScoreForSchool} from '../../helpers/'
import style from './style.scss';


export default ({school}) => {

    const score = calculateScoreForSchool(school);

    if(score > -1){
        const rating = Math.round(5*score);

        return (
            <div className={style.stars}>{Array(rating).fill().map(_ => '*').join('')}</div>
        )
    }

    return <div className={style.nostars}>No Ratings yet</div>

}