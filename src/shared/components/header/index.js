

import React from 'react';
import style from './style.scss'
import { Link } from 'react-router';

export default props => {
    
    
    return (
        <div className={style.header}>
            <Link className={style.logo} to="/">Schoolhub</Link>
            <Link className={style.flex4} to="/counter"></Link>
            <Link  to="/one">Inloggen</Link>
            <Link  to="/two">Info</Link>
            <Link  to="/two">Contact</Link>
        </div>
    );
}