

import React from 'react';
import style from './style.scss';


export default props => {


    let {increment, decrement, count} = props;
    return (
        <div className={style.counter}>
            <div className={style.count}>Count: {count}</div>
            <div>
                <button onClick={ decrement }>-</button>
                <button onClick={ increment }>+</button>
            </div>
        </div>
    )

}
