

import React from 'react'
import style from './style.scss';
import {connect} from 'react-redux';
import Stars from '../../components/stars';
import {withRouter} from 'react-router';
import _ from 'lodash';



const SearchResults = props => {

    const {router} = props;

    const schools = props.schools.map(


        school => {
            const visitReview = () => router.push(`/reviews/${school.id}`);
            const levels = _.uniq(school.subjects
                .map(subject => subject.level)).join(', ');



            return (
                <div onClick={visitReview} className={style.school} key={school.id}>

                    <div className={style.image}></div>

                    <div className={style.data}>
                        <div>
                            <Stars school={school}/>
                        </div>
                        <div>
                            <div className={style.schoolname}>{school.name}</div>
                            <div className={style.levels}>{levels}</div>
                        </div>
                    </div>

                </div>
            );

        }
    );




    return (
        <div className={style.container}>
            <div className={style.header}>{schools.length} scholen gevonden</div>
            <div>{schools}</div>
        </div>
    )

};

export default connect(
    state => ({ schools: state.schools  })
)(withRouter(SearchResults));