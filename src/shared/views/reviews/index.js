

import React from 'react';
import style from './style.scss';
import {connect} from 'react-redux';
import Stars from '../../components/stars';


const Reviews = props => {


    const {school} = props;
    const reviews = school.reviews.map(r => {

        return (
            <div key={r.id} className={style.review}>
                <div className={style.reviewMeta}>
                    <div>{r.user.anonym ? 'Anonymous' : r.user.name}</div>
                    <div>{new Date().toDateString()}</div>
                </div>
                <div className={style.reviewContent}>{r.content}</div>
            </div>
        );
    });

    return (
        <div>
            <div className={style.banner}>
                <div className={style.rating}>
                    <Stars school={school}/>
                </div>
                <div className={style.schoolName}>{school.name}</div>
            </div>
            <div className={style.data}>
                <div>TopDown</div>
                <div>{reviews}</div>
            </div>
        </div>
    )

};

const select = (state, props) => {

    const schoolID = props.routeParams.school_id;
    return {
        school: state.schools.find(s => s.id == schoolID)
    }

};

export default connect(select)(Reviews);