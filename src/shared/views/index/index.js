
import React from 'react';
import style from './style.scss';
import {connect} from 'react-redux';
import SearchBar from '../../components/search-bar';




const IndexView = props => {
    return  (
        <div className={style.component}>
            <div className={style.searchContainer}>
                <SearchBar/>
            </div>

        </div>
    );
};

export default connect()(IndexView);