

import React from 'react';
import style from './style.scss';

const Loading  = props => {
    return (
        <div className={style.loadingScreen}>
            Loading...
        </div>
    )
};

export default Loading;