



import initialData from './data/data.json';
import { combineReducers, compose, createStore, applyMiddleware } from 'redux';
import { autoRehydrate, persistStore} from 'redux-persist';
import ReduxThunk from 'redux-thunk';
import schools from './reducers/schools';



export default preloadedState => {

    const reducers = {
        schools,
        // listen for persist/rehydrate event from redux-persist, then set rehydrated to true
        // App needs this information, so that it can determine whether to render a loading screen or the actual app
        rehydrated: (state = false, action) => {
            if(action.type === 'persist/REHYDRATE'){
                return true
            }
            return state;
        }
    };

    const rootReducer = combineReducers(reducers);
    const enhancer = process.env.IS_BROWSER ?
        compose (
            applyMiddleware(ReduxThunk),
            autoRehydrate(),
            window.devToolsExtension ? window.devToolsExtension() : f => f,
        )
        :
        compose(applyMiddleware(ReduxThunk));

    const store =  createStore(rootReducer, {schools: initialData}, enhancer);
    persistStore(store, {whitelist: ['counter']});

    return store;

}