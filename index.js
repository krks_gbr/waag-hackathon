

//read
//https://medium.com/front-end-developers/handcrafting-an-isomorphic-redux-application-with-love-40ada4468af4#.9k4pr6z2s



require('dotenv').load();

//setup css-modules-require-hook
//https://github.com/css-modules/css-modules-require-hook
require('./cmrh.conf');

global.appRoot = require('path').resolve(__dirname);

if(process.env.NODE_ENV === 'development'){
    //enable on-the-fly es6 and react code transpilation
    require('babel-register');
    require('./src/server')({
        port: process.env.PORT,
        host: process.env.HOST,
        callback: require('./webpack/server')
    });
}

if(process.env.NODE_ENV === 'production') {
    require('./dist/server')({port: process.env.PORT, host: process.env.HOST});
}

