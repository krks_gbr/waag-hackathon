
const path = require('path');
const webpack = require('webpack');
const rootDir = path.dirname(require.main.filename);
const host = process.env.HOST || "0.0.0.0";
const port = process.env.WP_DEV_PORT || 3001;
const dist = path.resolve(rootDir, 'dist');
const autoprefixer = require('autoprefixer');

var config = require('../package').config;

module.exports = {
    devtool: "source-map",
    entry: [
        `webpack-dev-server/client?http://${host}:${port}`,
        "webpack/hot/dev-server",
        path.resolve(rootDir, 'src/client/main.js')
    ],
    output: {
        path: dist,
        filename: 'bundle.js',
        publicPath: `http://${host}:${port}/dist/`
    },

    module:{
        loaders:[

            { test: /\.(png|jpg|woff|woff2|eot|ttf|svg)$/, loader: 'url' },
            { test: /\.json$/, loader: 'json'},

            {
                loader: `style-loader!css?modules&localIdentName=${config.css}!sass`,
                test: /\.scss$/,
            },
            {
                test: /\.css$/,
                loader: `style-loader!css?modules&localIdentName=${config.css}`
            },

            {
                test: /\.jsx?$/,
                loaders: ["babel"],
                include: [path.resolve(rootDir, 'src/client/'), path.resolve(rootDir, 'src/shared/')]
            }
        ]
    },

    postcss: function(){
        return [autoprefixer];
    },

    plugins: [
        new webpack.optimize.DedupePlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                'IS_BROWSER': "'true'",
                'NODE_ENV': "'development'"
            }
        })
    ]

};