
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var webpack = require('webpack');
var StatsWriterPlugin = require("webpack-stats-plugin").StatsWriterPlugin;
var extractCSS = new ExtractTextPlugin("[hash].min.css");
const autoprefixer = require('autoprefixer');


module.exports = {
    entry: {
        main: "./src/client/main.js",
    },
    output: {
        path: "./dist/public",
        filename: "[hash].js",
    },
    module:{
        loaders:[

            { test: /\.(png|woff|woff2|eot|ttf|svg|otf)$/, loader: 'url-loader?limit=100000' },

            { test: /\.js$/, loader: "strip?strip[]=console.log" },

            {
                loader: extractCSS.extract("style", "css?modules&localIdentName=[hash:base64:5]!sass"),
                test: /\.scss$/,
            },

            {
                test: /\.css$/,
                loader: extractCSS.extract("style", "css?modules&localIdentName=[hash:base64:5]")
            },

            {
                test: /\.jsx?$/,
                loader: 'babel',
                include: [ path.resolve('./src/client/'), path.resolve('src/shared/') ]
            }
        ]
    },

    postcss: function(){
        return [autoprefixer];
    },

    plugins: [
        extractCSS,
        new webpack.DefinePlugin({
            'process.env': {
                'IS_BROWSER': "'true'",
                'NODE_ENV': "'production'"
            }
        }),

        new webpack.optimize.UglifyJsPlugin({comments: false}),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),

        // Write out stats file to build directory.
        new StatsWriterPlugin({
            transform: function (data) {
                return JSON.stringify({
                    main: data.assetsByChunkName.main[0],
                    css: data.assetsByChunkName.main[1]
                }, null, 4);
            }
        }),
    ]

};