import shortid from 'shortid';


const names = [
    "David de Haan",
    "Huib Weijenberg",
    "Klaas Mannen",
    "Henkie Koorman",
    "Jeroen op de Nijstat",
    "Sijmen Beuker",
    "Berend van 't Grote Laar",
    "Harm Bolhoeve",
    "Nicolaas van Hoorn",
    "Sander Kromhoff",
    "Valerie Harreveld",
    "Grietje Groenewold",
    "Mirte Vakkert",
    "Toos Asveld",
    "Jet Hekkers",
    "Marlijn Wilmes",
    "Martje van den Kerkdijk",
    "Marije Rebel",
    "Lieke Bastein",
    "Jooske Weteringe",
    "Henkjan Brus",
    "Egbert Soepenberg",
    "Sjef Rasch",
    "Karst-Jan Grooters",
    "Marthijn Elbers",
    "Arend Aldus",
    "Jurren Vosmeijer",
    "Sieb Blokvoort",
    "Jan-Hendrik Koebrugge",
    "Bas van de Pol",
    "Sanne Vlug",
    "Tatiana Brakke",
    "Eliza Horlig",
    "Marysa op de Haar",
    "Marije van den Belt",
    "Tatiana Wemekamp",
    "Eline van Eijk",
    "Carolien Mannesse",
    "Trienke van Vreden",
    "Jolien Rosenthal",
];


export default function users(){
    return names.map(name => ({
        id: shortid.generate(),
        reputationScore: Math.floor(Math.random()*100)/100,
        anonym: Math.random() < 0.9,
        name: name,
    }));
}