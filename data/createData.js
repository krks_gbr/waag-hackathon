



import texts from './reviewTexts';
import createUsers from './users';
import fs from 'fs';
import shortid from 'shortid';
import createSchools from './schools/createSchools';








const schools = createSchools();


const users = createUsers();

const output = schools.map((item)=>{

    item.id = shortid.generate();
    item.reviews = texts.map(text => ({
        id: shortid.generate(),
        user: users[Math.floor(Math.random()*users.length)],
        content: text,
        accuracy: Math.floor(Math.random()*100)/100,
        usefulness: Math.floor(Math.random()*100)/100,
    })).slice(0,~~(Math.random()*(texts.length-1)));


    return item;
});

fs.writeFile(`src/shared/store/data/data.json`, JSON.stringify(output, null, 4), err => {
    if(err){
        console.log("shit is fucked");
    }
});