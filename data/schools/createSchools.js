
import db from './utrecht-dataset.json';


export default function createSchools() {
    let schools = [];

    db.forEach(d => {

        const schoolName = d.school.toLowerCase().trim();
        const levelName = d.level.toLowerCase().trim();
        const subjectName = d.section.toLowerCase().trim();

        const school = schools.find(s => s.name === schoolName);

        if(!school){
            schools.push({
                name: schoolName,
                province: d.province.toLowerCase().trim(),
                city: d.city.toLowerCase().trim(),
                subjects: [],
            });

        } else {

            if(subjectName !== 'n.v.t.'){
                const subject = school.subjects.find( s => s.name === subjectName);

                if(!subject){
                    school.subjects.push({
                        name: subjectName,
                        level: levelName
                    });
                }
            }

        }

    });


    return schools.filter(d => d.subjects.length > 0);
}